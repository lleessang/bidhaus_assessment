# Python REST API

## Bidhaus Assessment: Python Rest API
- **Author:** Jay Liu
- **Date:** 05-01-2024

## Description
This project is a Python Flask REST API with in-memory SQLite3 database. It provides endpoints to perform operations on a simple product database.

## Table of Contents
1. [Data Model Design](#data-model-design)
2. [REST API Design](#rest-api-design)
3. [Usage](#usage)
4. [Error Handling](#error-handling)
5. [Setup and Running](#setup-and-running)
6. [Testing](#Testing)

<a name="data-model-design"></a>
## Data Model Design

The database contains a single table `products` with the following schema:

products(
id INTEGER PRIMARY KEY NOT NULL,
name TEXT NOT NULL,
description TEXT NOT NULL,
price REAL NOT NULL,
in_stock INTEGER NOT NULL
)

<a name="rest-api-design"></a>
## REST API Design

The following endpoints are available:

- **GET /products:** Returns all products' details.
- **POST /products:** Creates a new product.
- **GET /products/{id}:** Returns the details of a product with the specified ID.
- **PUT /products/{id}:** Updates details of a product with the specified ID.
- **DELETE /products/{id}:** Deletes a product with the specified ID.

<a name="usage"></a>
## Usage

1. **GET /products:** Retrieve all products.

2. **POST /products:** Create a new product.
    - Request Body:
        ```
        {
            "id": <int>,
            "name": <string>,
            "description": <string>,
            "price": <float>,
            "in_stock": <int>
        }
        ```

3. **GET /products/{id}:** Retrieve a specific product by its ID.

4. **PUT /products/{id}:** Update a specific product by its ID.
    - Request Body: Same as POST /products.

5. **DELETE /products/{id}:** Delete a specific product by its ID.

<a name="error-handling"></a>
## Error Handling

1. All database operations are wrapped with error handling to catch SQLite3 errors. 

2. Error responses are returned with appropriate HTTP status codes.

3. Input Validation is not implemented, since the data model is simple, and the schema is strict.
Handled by database operation error checks. 

<a name="setup-and-running"></a>
## Setup and Running

- This assessment is done in VS Code, using the following extensions:

1. flask-snippets
2. REST Client
3. Python Test Explorer for Visual Studio Code


- Run application:
```
python restapi_sqlite3.py 
```




<a name="Testing"></a>
## Testing

1. api_test.http file contains sample requests, run it with REST Client or any other API testing tool to check Request and Response

2. test_restapi.py file contains sample testing scenarios. Mock data not used.



--------------------------------------------------------
*README generated with the assistance of ChatGPT.*


