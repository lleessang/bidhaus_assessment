'''
Assessment: Python Rest API
Author: Jay Liu
Description: Python Flask Rest API with in-memory SQLite3
Date: 05-01-2024

'''

from flask import Flask, request, jsonify
import sqlite3
import logging

# create flask application
app = Flask(__name__)

# define decorator to handle database related errors
def handle_db_error(func):
    def wrapper(*args, **kwargs): # wrapper function can accept any number of positional and keyword arguments
        try:
            result = func(*args, **kwargs)
            return result
        except sqlite3.Error as e:
            logging.error(f"Database operation failed: {str(e)}")
    return wrapper

# apply decorator to db operation functions
def handle_db_errors():
    db_operations = [get_products, get_product_by_id, insert_product, update_product, delete_product]
    for operation in db_operations:
        # retrieves the function object associated with the name of the function from the global symbol table
        globals()[operation.__name__] = handle_db_error(operation)


'''
Data Model Design

products(
    id INTEGER PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    description TEXT NOT NULL,
    price REAL NOT NULL,
    in_stock INTEGER NOT NULL
    )

'''

# Create database table
@handle_db_error
def table_create(conn):
    cur = conn.cursor()
    cur.execute('''
                CREATE TABLE IF NOT EXISTS products(
                    id INTEGER PRIMARY KEY NOT NULL,
                    name TEXT NOT NULL,
                    description TEXT NOT NULL,
                    price REAL NOT NULL,
                    in_stock INTEGER NOT NULL
                )
            
    ''')
    conn.commit()
    cur.close()
  

# Function to get all products from the database
@handle_db_error 
def get_products():
    cur = conn.cursor()
    cur.execute("SELECT * FROM products")
    rows = cur.fetchall()
    conn.commit()
    cur.close()
    if rows:
        products = [{'id': row[0], 'name': row[1], 'description': row[2], 
                 'price': row[3], 'in_stock': row[4]} for row in rows]
        return products # or return None


# Function to get a product by its ID from the database
@handle_db_error 
def get_product_by_id(product_id):
    cur = conn.cursor()
    cur.execute("SELECT * FROM products WHERE id = ?", (product_id,))
    row = cur.fetchone()
    conn.commit()
    cur.close()
    if row:
        return {'id': row[0], 'name': row[1], 'description': row[2], 'price': row[3], 'in_stock': row[4]}
    
# Function to insert a new product into the database    
@handle_db_error 
def insert_product(product):
    cur = conn.cursor()
    cur.execute("INSERT INTO products (id, name, description, price, in_stock) VALUES (?, ?, ?, ?, ?)",
                   (product['id'], product['name'], product['description'], product['price'], product['in_stock']))
    
    product_id = product['id']
    conn.commit()
    cur.close()
    return get_product_by_id(product_id) # show inserted product

# Function to update an existing product in the database
@handle_db_error 
def update_product(product):
    cur = conn.cursor()
    result = cur.execute("UPDATE products SET name = ?, description = ?, price = ?, in_stock = ? WHERE id = ?",
                   (product['name'], product['description'], product['price'], product['in_stock'], product['id']))
    conn.commit()
    cur.close()
    if result.rowcount != 0:
        return get_product_by_id(product['id']) # show updated product

# Function to delete a product from the database
@handle_db_error 
def delete_product(product_id):
    cur = conn.cursor()
    result = cur.execute("DELETE FROM products WHERE id = ?", (product_id,))
    conn.commit()
    cur.close()
    if result.rowcount != 0:
        return {'message': f'Product with ID {product_id} deleted successfully'}



'''
REST API Design

GET request to /products returns all products' details
POST request to /products creates a new product

GET request to /products/{id} returns the details of product id {id}
PUT request to /products/{id} updates details of product id {id}
DELETE request to /products/{id} delete product id {id}

------------------------------------------------------------------

Input Validation is not implemented, since the data model is simple, and the schema is strict.
Handled by database operation error checks.   

'''


# REST API endpoint for populating the database with initial data
@app.route('/db_populate', methods = ['POST'])
def db_populate():
    try:
        cur = conn.cursor()
        cur.execute("DROP TABLE IF EXISTS products")
        conn.commit()
        cur.close()
        table_create(conn)
        products = [
            {"id": 1, "name": "Nikon Z7", "description": "Nikon Mirrorless Camera released in 2018", "price": 2099, "in_stock": 10},
            {"id": 2, "name": "Nikon Z5", "description": "Nikon Mirrorless Camera released in 2020", "price": 1099, "in_stock": 5}
        ] # sample initial data in db
        for product in products:
            result = insert_product(product)
        return jsonify({'message':'Data Populated'}), HTTP_CREATED       
    except Exception as e:
        logging.debug(f"Failed to populate the database: {str(e)}")
        return jsonify({'error': 'Failed to populate database'}), HTTP_INTERNAL_SERVER_ERROR



# REST API endpoint to retrieve all products
@app.route('/products', methods=['GET'])
def get_all_products():
    try:
        result = get_products()
        if not result: 
            return jsonify({'error': 'No products found'}), HTTP_NOT_FOUND
        else:
            return jsonify(result), HTTP_OK
    except Exception as e:
        logging.error(f"Failed to retrieve all products: {str(e)}")
        return jsonify({'error': 'Failed to retrieve all products'}), HTTP_INTERNAL_SERVER_ERROR



# REST API endpoint to create a new product
@app.route('/products', methods=['POST'])
def create_product():
    content = request.get_json()
    product_id = content['id']
    try:
        content = request.get_json()
        result = insert_product(content)
        if not result: 
            return jsonify({'error': 'Cannot create new product'}), HTTP_INTERNAL_SERVER_ERROR
        else:
            return jsonify(result), HTTP_CREATED
    except Exception as e:
        logging.error(f"Failed to create product: {str(e)}")
        return jsonify({'error': f'Failed to create product with ID {product_id}'}), HTTP_INTERNAL_SERVER_ERROR



# REST API endpoint to retrieve a specific product by id
@app.route('/products/<int:product_id>', methods=['GET'])
def get_product(product_id):
    try:
        result = get_product_by_id(product_id)
        if not result:
            return jsonify({'error': f'Product with ID {product_id} not found'}), HTTP_NOT_FOUND
        else:
            return jsonify(result),HTTP_OK
    except Exception as e:
        logging.error(f"Failed to retrieve product: {str(e)}")
        return jsonify({'error': f'Failed to retrieve product with ID {product_id}'}), HTTP_INTERNAL_SERVER_ERROR



# REST API endpoint to update a specific product by id
@app.route('/products/<int:product_id>', methods=['PUT'])
def update_product_endpoint(product_id):
    try:
        content = request.get_json()
        content['id'] = product_id
        result = update_product(content)
        if not result: 
            return jsonify({'error': 'Failed to update product'}), HTTP_NOT_FOUND
        else:
            return jsonify(result),HTTP_OK
    except Exception as e:
        logging.error(f"Failed to update product: {str(e)}")
        return jsonify({'error': f'Failed to update product with ID {product_id}'}), HTTP_INTERNAL_SERVER_ERROR



# REST API endpoint to delete a specific product by id
@app.route('/products/<int:product_id>', methods=['DELETE'])
def delete_product_endpoint(product_id):
    try:
        result = delete_product(product_id)
        if not result: 
            return jsonify({'error': f'Failed to delete product with ID {product_id}'}), HTTP_NOT_FOUND
        else:
            return jsonify(result),HTTP_OK        
    except Exception as e:
        logging.error(f"Failed to delete product: {str(e)}")
        return jsonify({'error': 'Failed to delete product'}), HTTP_INTERNAL_SERVER_ERROR




if __name__ == '__main__':
    
    # Initialize logging
    # display logs in console as well as in the log file
    logging.basicConfig(filename='./restapi_sqlite3.log', level=logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    logging.getLogger().addHandler(consoleHandler)

    # Establish database connection
    # In-memory DB
    conn = sqlite3.connect(':memory:', check_same_thread=False)   

    # Initialize the database
    table_create(conn) 

    # Define HTTP response codes as constants
    HTTP_OK = 200
    HTTP_CREATED = 201
    HTTP_NOT_FOUND = 404
    HTTP_INTERNAL_SERVER_ERROR = 500

    app.run(debug=True)
