'''
Please note:
Use this file as test script examples. 
Mock data is not used, connecting to the same in-memory DB

'''

import unittest
import requests

class ApiTest(unittest.TestCase):
    API_URL = "http://127.0.0.1:5000"
    DATA_POPULATE = "{}/db_populate".format(API_URL)
    PRODUCTS_URL = "{}/products".format(API_URL)
    PRODUCT_OBJ = {
        "id" : 4,
        "name": "Nikon Z6",
        "description": "Nikon Mireless Camera released on 2018",
        "price": 1699,
        "in_stock": 5
    }

    NEW_PRODUCT_OBJ = {
        "id" : 4,
        "name": "Nikon Z6ii",
        "description": "Nikon Mireless Camera released on 2020",
        "price": 2199,
        "in_stock": 7
    }

    def _get_each_product_url(self, product_id): 
        return "{}/{}".format(ApiTest.PRODUCTS_URL, product_id)
    
    def test_0_data_population(self):
        r = requests.post(ApiTest.DATA_POPULATE)
        self.assertEqual(r.status_code, 201)

    # GET request to /products returns the details of all products
    def test_1_get_all_products(self):
        r = requests.get(ApiTest.PRODUCTS_URL)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(r.json()), 2)

    # POST request to /products to create a new product
    def test_2_add_new_product(self):
        r = requests.post(ApiTest.PRODUCTS_URL, json=ApiTest.PRODUCT_OBJ)
        self.assertEqual(r.status_code, 201)

    def test_3_get_new_product(self):
        product_id = self.PRODUCT_OBJ['id']
        r = requests.get(self._get_each_product_url(product_id))
        self.assertEqual(r.status_code, 200)
        self.assertDictEqual(r.json(), ApiTest.PRODUCT_OBJ)

    # PUT request to /products/product_id
    def test_4_update_existing_product(self):
        product_id = self.NEW_PRODUCT_OBJ['id']
        r = requests.put(self._get_each_product_url(product_id),
                         json=ApiTest.NEW_PRODUCT_OBJ)
        self.assertEqual(r.status_code, 200)

    # GET request to /products/product_id
    def test_5_get_new_product_after_update(self):
        product_id = self.NEW_PRODUCT_OBJ['id']
        r = requests.get(self._get_each_product_url(product_id))
        self.assertEqual(r.status_code, 200)
        self.assertDictEqual(r.json(), ApiTest.NEW_PRODUCT_OBJ)

    # DELETE request to /products/product_id
    def test_6_delete_product(self):
        product_id = self.PRODUCT_OBJ['id']
        r = requests.delete(self._get_each_product_url(product_id))
        self.assertEqual(r.status_code, 200)

    @unittest.expectedFailure
    def test_7_get_new_product_after_delete(self):
        product_id = self.PRODUCT_OBJ['id']
        r = requests.get(self._get_each_product_url(product_id))
        self.assertEqual(r.status_code, 200)
 